# ImagoMundiMei

Imago Mundi Mei was a free workshop that had as its goal the graphic research of two little-known techniques: typometry and psychogeography. It was proposed by the Velvetyne Type Foundry as a part of the Fanzines! festival and in partnership with the Maison du Libre et des Communs.

During this workshop, participants created together typographic characters in a square format which, placed side by side (typometry) allows us to draw emotional, conceptual, psychological landscapes (psychogeography).

![specimen1](documentation/specimen/specimen-imagomundimei-01.png)

Instructions: in order to avoid white lines in the composed landscape, we recommend to set the leading of the text with the same value (in points) as the text body size. For instance, if your text is set at 60 points, then your leading should be 60 pt too, and everything will look pretty neat. Keep tracking at 0 points. Fantastic!

## Specimen

![specimen2](documentation/specimen/specimen-imagomundimei-02.png)


## License

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository follows the Sample font repository based on the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at :
- https://github.com/djrrb/Sample-Font-Repository
- https://github.com/raphaelbastide/Unified-Font-Repository
